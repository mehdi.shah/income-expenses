from django.db import models
from authentication.models import User




class Expenses(models.Model):

    CATEGORY_OPTIONS = (
        ('ONLINE_SERVICES', 'ONLINE_SERVICES'),
        ('TRAVEL', 'TRAVEL'),
        ('FOOD', 'FOOD'),
        ('RENT', 'RENT'),
        ('OTHERS', 'OTHERS')
    )

    category = models.CharField(choices=CATEGORY_OPTIONS, max_length=255)
    amount = models.DecimalField(
        max_digits=10, decimal_places=2, max_length=255)
    description = models.TextField(max_length=1000)
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)
    date = models.DateField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=255 , blank=False , null=False)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.CharField(max_length=255 , blank=False , null=False)
    deleted_at = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=255 , blank=True , null=True)
    is_deleted = models.BooleanField(default=False)


    def __str__(self):
        return str(self.owner)+'s income'
