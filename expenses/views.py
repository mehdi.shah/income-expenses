 
from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from expenses.serializers import ExpensesSerializer
from expenses.models import Expenses
from rest_framework import permissions
from expenses.permissions import IsOwner


class ExpenseListAPIView(ListCreateAPIView):
    serializer_class = ExpensesSerializer
    queryset = Expenses.objects.filter(is_deleted=False).order_by("-updated_at")
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)


class ExpenseDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExpensesSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    queryset = Expenses.objects.filter(is_deleted=False).order_by("-updated_at")
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)
