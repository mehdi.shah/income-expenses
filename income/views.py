from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from income.serializers import IncomeSerializer
from income.models import Income
from rest_framework import permissions
from income.permissions import IsOwner


class IncomeListAPIView(ListCreateAPIView):
    serializer_class = IncomeSerializer
    queryset = Income.objects.filter(is_deleted=False).order_by("-updated_at")
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)


class IncomeDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = IncomeSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwner,)
    queryset = Income.objects.filter(is_deleted=False).order_by("-updated_at")
    lookup_field = "id"

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)